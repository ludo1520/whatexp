package fr.ludo1520.whatexp;


/*
Copyright 2014 Ludovic Gaudichet

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
*/


import java.io.File;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

// news : add sharing function, bug correction, 

public class WhatExp extends Activity implements
ActionBar.TabListener, functListFragment.funcListListener, OnItemSelectedListener {

    public static final String PREFS_NAME = "WhatExpPref";
    public static final String N_FUNC_NAME = "NumFunc";

	MathFuncParser mF = new MathFuncParser();
	EditText mEditFunc;
	Button mEvalButton;
	TextView mTextResult;
	Spinner mEditSpinner;
	
	private functListFragment mFuncList;
	private CalcGraphFragment mCalcGraph;
	private SetConstantFragment mConstFrag;

	String mExExpr;
	
	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current tab position.
	 */
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	    SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

	    String str,strDouble;
	    for (int i=0; i<MathFuncParser.NConst;++i) {
	    	str = "const" + i;
	    	strDouble = settings.getString(str, "0");
	    	MathFuncParser.SetConst(i,Double.parseDouble(strDouble));
	    }
	    
		setContentView(R.layout.activity_main);
	
    	mEditFunc = (EditText) findViewById(R.id.edit_function);
        mEditFunc.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId==EditorInfo.IME_ACTION_DONE) Eval(null);
                return false;
            }
        });

    	mEvalButton = (Button) findViewById(R.id.evalButton);
    	mTextResult = (TextView) findViewById(R.id.viewCalcResult);
    	mEditSpinner = (Spinner) findViewById(R.id.edit_spinner);
    	mEditSpinner.setOnItemSelectedListener(this);
    	ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(this,
    	        R.array.editChar_array, android.R.layout.simple_spinner_item);
    	spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	mEditSpinner.setAdapter(spinnerAdapter);
    	
		mExExpr="";
		mFuncList = new functListFragment();
		mCalcGraph = new CalcGraphFragment();
		mConstFrag = new SetConstantFragment();
		
		mCalcGraph.mXmin = settings.getFloat("xMin",-5);
		mCalcGraph.mXmax = settings.getFloat("xMax",5);
		mCalcGraph.mYmin = settings.getFloat("yMin",-5);
		mCalcGraph.mYmax = settings.getFloat("yMax",5);
		
	    int nFunc = settings.getInt(N_FUNC_NAME,0);
	    for (int i=0; i<nFunc;++i) {
	    	str = "colorToDraw" + i;
	    	int colorToDraw = settings.getInt(str,0);
	    	str = "func" + i;
	    	mFuncList.AddToEnd(settings.getString(str,""),colorToDraw);
	    }

		// getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    // If your minSdkVersion is 11 or higher, instead use:
		final ActionBar actionBar = getActionBar();
	    //actionBar.setDisplayHomeAsUpEnabled(true);// if not the main activity, allows going back
		
		// Set up the action bar to show tabs.
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		// For each of the sections in the app, add a tab to the action bar.
		actionBar.addTab(actionBar.newTab().setText(R.string.title_funcList)
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText(R.string.title_graph)
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText(R.string.title_setConst)
				.setTabListener(this));
	}

    @Override
    protected void onStop() {
       super.onStop();

      SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
      SharedPreferences.Editor editor = settings.edit();
	    String str, doubleStr;
	    for (int i=0; i<MathFuncParser.NConst;++i) {
	    	str = "const" + i;
	    	doubleStr = String.valueOf(MathFuncParser.GetConst(i));
	    	editor.putString(str,doubleStr);
	    }
	    editor.putFloat("xMin",mCalcGraph.mXmin);
	    editor.putFloat("xMax",mCalcGraph.mXmax);
	    editor.putFloat("yMin",mCalcGraph.mYmin);
	    editor.putFloat("yMax",mCalcGraph.mYmax);

	    int nFunction = mFuncList.Nfunc();
    	editor.putInt(N_FUNC_NAME,nFunction);

	    for (int i=0; i<nFunction;++i) {
	    	str = "func" + i;
	    	editor.putString(str,mFuncList.GetFuncStr(i));
	    	str = "colorToDraw"+i;
	    	editor.putInt(str, mFuncList.GetColorDraw(i));

	    }
      editor.commit();
    }
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current tab position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current tab position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	//
	// Pour récupérer les settings qui ont éventuellement
	// été fait dans l'activité settings ...
	
//    public static int CODE_Settings = 1; 
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//    	//if (resultCode == RESULT_OK) {
//    		Bundle extra = data.getExtras();
//    		if (requestCode == CODE_Settings) {
//    			boolean clearFun = extra.getBoolean("ClearFunc");
//    			if (clearFun) mFuncList.Clear();
//    		}
//    	//}
//    	
//    }
	

    public void Eval(View view) {
     	
    	String expr = mEditFunc.getText().toString();
    	
    	if (!expr.equals(mExExpr)) {

       		mExExpr = expr;
    		mF.Define(expr);
    		if (mF.M_DefineError==0) {
    			if (mF.FunctionOfX()) {
        			mFuncList.Add(expr,0);
        			mTextResult.setText("");
    			} else { // if not a function of x
    				String strResult = String.valueOf(mF.Eval(0));
    				if (mF.M_error==0) {
            			mFuncList.Add(expr,0);
            			mTextResult.setText(strResult);
    				} else {
    					mTextResult.setText("error");
    				}
    			}
    		} else { // if mF.M_DefineError !=0
    			mTextResult.setText("error");
    		}
		
    	}
    }
	
    /***************************************************
     *  menu
     * 
     * https://developer.android.com/guide/topics/ui/actionbar.html#ActionProvider
     * 
     ***************************************************/  
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.calc_action_settings:
	        	Intent intent = new Intent(this, Settings.class);
	        	//startActivityForResult(intent,CODE_Settings);
	        	startActivity(intent);
	            return true;
	        case R.id.action_share :
	        	if (mCalcGraph.isVisible()) {
	        		File share = mCalcGraph.savePNG();
	        		Intent intentShare = new Intent(android.content.Intent.ACTION_SEND);
	        		intentShare.setType("image/png");
	        		intentShare.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+share));
	        		try {
	        			startActivity(Intent.createChooser(intentShare, "Share picture"));
	        		} catch (Exception e) {
	        		}
	        		
	        	} else if (mFuncList.isVisible()) {
	        		
	        		String str = mFuncList.getSelectedExp();
	        		if (str!="") {
	        			Intent intentShare = new Intent(android.content.Intent.ACTION_SEND);
	        			intentShare.setType("text/plain");
	        			intentShare.putExtra(Intent.EXTRA_TEXT, str);
	        			startActivity(Intent.createChooser(intentShare, "Share selected exp."));
	        			//getResources().getText(R.string.send_to)
	        		}
	        	}
	        	
	        	return true;

	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
    
	
	/***************************************************
	 *  implements ActionBar.TabListener
	 * 
	 ***************************************************/
	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, show the tab contents in the
		// container view.
		
		if (tab.getPosition()==0) {
			mEditFunc.setVisibility(View.VISIBLE);
			mTextResult.setVisibility(View.VISIBLE);
			mEvalButton.setVisibility(View.VISIBLE);
			mEditSpinner.setVisibility(View.VISIBLE);
			getFragmentManager().beginTransaction()
			.replace(R.id.containerCalcB, mFuncList).commit();
		}
		else if (tab.getPosition()==1) {
			mEditFunc.setVisibility(View.GONE);
			mTextResult.setVisibility(View.GONE);
			mEvalButton.setVisibility(View.GONE);
			mEditSpinner.setVisibility(View.GONE);
			mCalcGraph.ClearFunc();
			int comptDraw = 0;
			for (int i=mFuncList.Nfunc()-1; i>=0;--i) {
				int color = mFuncList.GetColorDraw(i);
				if (color!=0) {
					mCalcGraph.AddFunction(mFuncList.GetFuncStr(i),color);
					++comptDraw;
				}
			}
			if (comptDraw==0) {
				mCalcGraph.mXmin = -5; mCalcGraph.mXmax = 5;
				mCalcGraph.mYmin = -5; mCalcGraph.mYmax = 5;
			}		
			getFragmentManager().beginTransaction()
			.replace(R.id.containerCalcB, mCalcGraph).commit();
		}
		else {
			mEditFunc.setVisibility(View.GONE);
			mTextResult.setVisibility(View.GONE);
			mEvalButton.setVisibility(View.GONE);
			mEditSpinner.setVisibility(View.GONE);
			getFragmentManager().beginTransaction()
			.replace(R.id.containerCalcB, mConstFrag).commit();
		}
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
	
	/***************************************************
	 *  implements functListFragment.funcListListener
	 * 
	 ***************************************************/
	//private static final String TAG = "calculatorB";

	@Override
	public void onFuncSelected(String exp, int position) {
		//Log.d(TAG, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ");
		mEditFunc.setText(exp);
		mTextResult.setText("");
	}

	
    /*********************************************************
     * implements OnItemSelectedListener (spinner)
     *
     * Spinner for inserting special characters
     *
     ********************************************************/
	
	@Override
	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
		Editable str = mEditFunc.getText();
		CharSequence cs = (CharSequence) parent.getItemAtPosition(pos);
		int stopSelection=mEditFunc.getSelectionEnd();
		str.insert(stopSelection, cs);
		parent.setSelection(0);
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {		
	}

}
