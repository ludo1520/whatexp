package fr.ludo1520.whatexp;


/*
Copyright 2014 Ludovic Gaudichet

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
*/


import java.util.ArrayList;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;


/*  Constantes physiques :
 *  http://pdg.lbl.gov/2013/reviews/rpp2013-rev-phys-constants.pdf
 *  http://pdg.lbl.gov/2013/reviews/rpp2013-rev-astrophysical-constants.pdf
 *  J. Beringer et al. (Particle Data Group), Phys. Rev. D86, 010001 (2012)
 * 
 * 
 */


public class SetConstantFragment extends Fragment implements OnItemSelectedListener,
	PublishedConstAdapter.Listener {
	
	EditText mEditConst;
	Spinner mSpinner;
	int mPosConst=0;
	String constName[]={"a =","b =","c =","d =","e =","f =","g =","h =", "i =",
			"j =","k =","l =","m =","n =","o =","p =","q =","r =",
			"s =","t =","u =","v =","w =","y =","z="};
	ArrayList<String> mSpinnerArray;
	ArrayAdapter<String> mAdapter;
	ListView mPublishedConstList;
	public PublishedConstAdapter mPublishedAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_set_constant, container, false);
    }
 
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	
    	mEditConst = (EditText) getView().findViewById(R.id.edit_const);
    	mSpinner = (Spinner) getView().findViewById(R.id.constLetter_spinner);
    	mSpinner.setOnItemSelectedListener(this);
    	final Button button = (Button) getView().findViewById(R.id.set_const_button);
    	
    	mSpinnerArray = new ArrayList<String>();
    	for (int i=0; i<MathFuncParser.GetNconst(); ++i)
    		mSpinnerArray.add(constName[i]+String.valueOf(MathFuncParser.GetConst(i)));
    			
    	mAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.constspinner_item, mSpinnerArray);
    	//android.R.layout.simple_spinner_item
    	mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	mSpinner.setAdapter(mAdapter);
    	
    	button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	String str = mEditConst.getText().toString();
            	MathFuncParser f = new MathFuncParser();
            	f.Define(str);
            	double result = f.Eval(0);
            	mSpinnerArray.set(mPosConst, constName[mPosConst]+String.valueOf(result));
            	MathFuncParser.SetConstant(mPosConst, result);
            	mAdapter.notifyDataSetChanged();
            }
        });

    	mPublishedAdapter = new PublishedConstAdapter(getActivity());
    	mPublishedAdapter.SetListener(this);
    	mPublishedConstList = (ListView)getView().findViewById(R.id.ListConst);
    	mPublishedConstList.setAdapter(mPublishedAdapter);

    	super.onActivityCreated(savedInstanceState);
    }

    
    /*********************************************************
     * implements OnItemSelectedListener (spinner)
     * 
     ********************************************************/

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, 
            int pos, long id) {
		// An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
		mPosConst=pos;
		//mEditConst.setText( String.valueOf(MathFuncParser.GetConst(pos)) );
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {		
	}
	
    /*********************************************************
     * implements onPublishedConstClick
     * 
     ********************************************************/

	@Override
	public void onPublishedConstClick(double val) {
		mEditConst.setText( String.valueOf(val) );
	}
}
