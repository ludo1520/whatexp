package fr.ludo1520.whatexp;

/*
Copyright 2014 Ludovic Gaudichet

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

import java.util.Locale;


/* 
 * A class used as a mathematical function defined by a string
 * 
 *  Ludovic Gaudichet 
 */


public class MathFuncParser {
	
	static int Nmax=35; // nombre d'opérations maximum
	static int NConst = 25;
	static double constant[] = new double[NConst];
	static double M_Ans;

	public int mColor=0;
	
	enum operations { op_plus, op_moins, op_mul, op_div, rc, op_sqr, op_sin,
        op_cos, op_tan, op_ln, op_log, op_exp, op_xpy, op_arcs,
        op_arcc, op_arct, op_ch, op_sh, op_th, op_argsh, op_argch,
        op_argth, op_abs, op_frac, op_int, op_fact, op_ANP, op_CNP,
        op_d_r};
        
    int ordre1[] = new int[Nmax];
    int ordre2[] = new int[Nmax];
    int x_places[] = new int[Nmax+1];
    operations operateurs[] = new operations[Nmax];
    double valr[] = new double[Nmax+1]; // utilisé dans Eval
        
    public int M_error;
    public int M_DefineError;
    int N, Nx;
    double valeurs[] = new double[Nmax+1]; // rempli dans Define
    
    public MathFuncParser() {
    }
    
    static void SetConst(int i, double val) {
    	if ((i>=0)&&(i<NConst)) constant[i] = val;
    }
    
    static int GetNconst() {return NConst;}
    
    static double GetConst(int i) {
    	if ((i>=0)&&(i<NConst)) return constant[i];
    	else return 0;
    }
    
    //====================================================
    //
    //==================== Define ========================
    //
    //====================================================
    
    int mI_op;
	operations mOp[] = new operations[Nmax];
	int mPriorite[] = new int[Nmax+1];

    private void mettre_op( operations oper,int prior, int parenth) {
    	
    	if (mI_op==Nmax) M_error=200;
    	else {
    		mOp[mI_op] = oper;
    		mPriorite[mI_op++] = prior+parenth;	
    	}    
    }
    
	public void Define(String exp) {
		
		mI_op = 0;
		M_error = 0;
		int L= exp.length(), compt=0, compt2=0, i_val=0;
		char ch;
		int parenthese=0;
		String str;
	    StringBuffer expr= new StringBuffer( exp.toUpperCase(Locale.ENGLISH) );
	    expr.append(' ');

	    //=== Ajouter un 0 avant un - ou un + au début de l'expression 
	    while ( expr.charAt(compt)==' ' ) ++compt;
	    if (( expr.charAt(compt)=='-' )||( expr.charAt(compt)=='+' )) {
	    	expr.insert(compt, '0');
	        ++L;
	    };
	     
	    //=== Ajouter un 0 avant un - ou un + au début d'une parenthèse
	    while (compt<L)
	    	 if ((expr.charAt(compt)=='(') || (expr.charAt(compt)=='[') ||
	    		 (expr.charAt(compt)=='{')  )  {
	    		 while (expr.charAt(++compt)==' ');
	             if ( (expr.charAt(compt)=='-')||(expr.charAt(compt)=='+') )
	             {
	                expr.insert(compt, '0');
	                ++L;
	             };
	          } else ++compt;
	    //fin de l"arrangement de l"expr	     
	    //System.out.println(expr.toString());
    
	    Nx=0;
	    boolean IsInit[] = new boolean[Nmax+1];
	    for (compt=0; compt<=Nmax; ++compt) IsInit[compt] = false;
	    compt=0;
	    for (;compt2<mPriorite.length; ++compt2) mPriorite[compt2]=0;

	    //=== 
	    while ((compt<L) && (M_error==0)) {
	    	 
	         ch=expr.charAt(compt);
	         switch (ch) {
	         
	          case 39: compt++; // ?
	                   while ( (expr.charAt(compt++)!=39)&&(compt<L) );
	                   break;
	          case '{':  
	          case '(':
	          case '[': if (mI_op+1==i_val) { // multiplication abrégée devant une parenthèse
	        	  			mOp[mI_op]=operations.op_mul;
	        	  			mPriorite[mI_op++]=4+parenthese;
	          			}
	          			parenthese+=10;
	          			compt++;
	          			break;
	          case '}':		
	          case ')':
	          case ']': if (parenthese != 0) parenthese-=10;
	                    compt++;
	                    break;
	          case ' ': compt++;
	                    break;
	          case '+': /*if (!((mI_op==i_val)&&((mOp[mI_op]==operations.op_plus)||
	                                           (mOp[mI_op]==operations.op_moins))))*/
	                       mettre_op(operations.op_plus, 1,parenthese);
	                     compt++;
	                     break;
	          case '-': /*if ((mI_op==i_val)&&((mOp[mI_op]==operations.op_plus)||
                      						(mOp[mI_op]==operations.op_moins)))
	        	  			if (mOp[mI_op]==operations.op_plus)
	        	  				mOp[mI_op]=operations.op_moins;
	        	  			else
	        	  				mOp[mI_op]=operations.op_plus;
	          			else*/
	          				mettre_op(operations.op_moins,1,parenthese);
	          			compt++;
	          			break;
	          case '×':
	          case '*': mettre_op(operations.op_mul,2,parenthese);
              			compt++;
              			break;
	          case '÷':
	          case '/': mettre_op(operations.op_div,2,parenthese);
              			compt++;
              			break;
	          case '^': mettre_op(operations.op_xpy,5,parenthese);
              			compt++;
              			break;	          			
	          case	'°': mettre_op(operations.op_d_r,6,parenthese);          			
	          			compt++;
	          			i_val++;
	          			break;
	          case '!': mettre_op(operations.op_fact,6,parenthese);
              			compt++;
              			i_val++;
              			break;	
	          case '²' : mettre_op(operations.op_sqr,6,parenthese);
              			compt++;
              			i_val++;
              			break;
	          case '√' : mettre_op(operations.rc,6,parenthese);
    					compt++;
	                    i_val++;
	                    break;			
	          case '0': case '1': case '2': case '3': case '4': case '5':
	          case '6': case '7': case '8': case '9':
	                    compt2=compt;
	                    while (((expr.charAt(compt)>='0')&&(expr.charAt(compt)<='9'))
	                           ||(expr.charAt(compt)=='.'))
	                          compt++;
	                    if (expr.charAt(compt)=='E')
	                    {
	                      compt++;
	                      if ((expr.charAt(compt)=='+')||(expr.charAt(compt)=='-')) compt++;
	                      if ((expr.charAt(compt)>='0')&&(expr.charAt(compt)<='9'))
	                         while ((expr.charAt(compt)>='0')&&(expr.charAt(compt)<='9'))
	                               compt++;
	                      else M_error= 255;
	                    };
	                    str = expr.substring(compt2,compt);
	                	try {
	                      valeurs[i_val] = Double.parseDouble(str);
	                	} catch(NumberFormatException nfe) {
	                		M_error= 254;
	                	}
	                	IsInit[i_val++]=true;
	           	     	//System.out.println("string = "+str);              
	           	     	//System.out.println("val = "+valeurs[i_val]);
	                   
	                    break;	          			
	          case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
	          case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
	          case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
	          case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
	          case 'Y': case 'Z':               
	        	  		compt2=compt;
	        	  		while ((expr.charAt(compt)>='A')&&
	        	  				(expr.charAt(compt)<='Z')) compt++;
	                    str = expr.substring(compt2,compt);
	                    
	           	     	//System.out.println("letters :"+str);

	                    // multiplication abrégée devant X ou une fonction
	                    if (mI_op+1==i_val)
	                    	mettre_op( operations.op_mul,4,parenthese);
	                    
	                    // x ou une constante 
	                    if (str.length()==1) {
	                    	switch (str.charAt(0)) {
	                    		case 'X': x_places[Nx++]=i_val; break;
	                            case 'A': valeurs[i_val]=constant[0]; break;
	                            case 'B': valeurs[i_val]=constant[1]; break;
	                            case 'C': valeurs[i_val]=constant[2]; break;
	                            case 'D': valeurs[i_val]=constant[3]; break;
	                            case 'E': valeurs[i_val]=constant[4]; break;
	                            case 'F': valeurs[i_val]=constant[5]; break;
	                            case 'G': valeurs[i_val]=constant[6]; break;
	                            case 'H': valeurs[i_val]=constant[7]; break;
	                            case 'I': valeurs[i_val]=constant[8]; break;
	                            case 'J': valeurs[i_val]=constant[9]; break;
	                            case 'K': valeurs[i_val]=constant[10]; break;
	                            case 'L': valeurs[i_val]=constant[11]; break;
	                            case 'M': valeurs[i_val]=constant[12]; break;
	                            case 'N': valeurs[i_val]=constant[13]; break;
	                            case 'O': valeurs[i_val]=constant[14]; break;
	                            case 'P': valeurs[i_val]=constant[15]; break;
	                            case 'Q': valeurs[i_val]=constant[16]; break;
	                            case 'R': valeurs[i_val]=constant[17]; break;
	                            case 'S': valeurs[i_val]=constant[18]; break;
	                            case 'T': valeurs[i_val]=constant[19]; break;
	                            case 'U': valeurs[i_val]=constant[20]; break;
	                            case 'V': valeurs[i_val]=constant[21]; break;
	                            case 'W': valeurs[i_val]=constant[22]; break;
	                            case 'Y': valeurs[i_val]=constant[23]; break;
	                            case 'Z': valeurs[i_val]=constant[24]; break;
	                            default : M_error =253;
	                    	} // end switch (x ou constante)
	                    	IsInit[i_val++]=true; //i_val++;
	                    } // end if (x ou constante)
	                    else if (str.equals("PI")) {
	                    	IsInit[i_val]=true;
	                    	valeurs[i_val++]=Math.PI;
	                    }
	                    else if (str.equals("ANS")) {
	                    	IsInit[i_val]=true;
	                    	valeurs[i_val++]=M_Ans;
	                    }
	                    else if (str.equals("SQRT"))
	                        { mettre_op(operations.rc,4,parenthese);
	                          i_val++;
	                        }
	                    else if (str.equals("SIN"))
	                        { mettre_op(operations.op_sin,4,parenthese);
	                          i_val++;
	                        }
	                    else if (str.equals("COS"))
	                        { mettre_op(operations.op_cos,4,parenthese);
	                          i_val++;
	                        }
	                    else if (str.equals("TAN"))
	                        { mettre_op(operations.op_tan,4,parenthese);
	                          i_val++;
	                        }
	                    else if (str.equals("LN")) 
	                        { mettre_op(operations.op_ln,4,parenthese);
	                          i_val++;
	                        }
	                    else if (str.equals("LOG")) 
	                        { mettre_op(operations.op_log,4,parenthese);
	                          i_val++;
	                        }
	                    
	                    else if (str.equals("EXP")) 
	                    	{ mettre_op(operations.op_exp,4,parenthese);
	                    	i_val++;
	                    	}
	                    else if (str.equals("ARCSIN")) 
	                    	{ mettre_op(operations.op_arcs,4,parenthese);
	                    	i_val++;
	                    	}
	                    else if (str.equals("ARCCOS")) 
	                    	{ mettre_op(operations.op_arcc,4,parenthese);
	                    	i_val++;
	                    }
	                    else if (str.equals("ARCTAN")) 
	                    	{ mettre_op(operations.op_arct,4,parenthese);
	                    	i_val++;
	                    	}
	                    else if (str.equals("CH")) 
	                    	{ mettre_op(operations.op_ch,4,parenthese);
	                    	i_val++;
	                    	}
	                    else if (str.equals("SH")) 
	                    { mettre_op(operations.op_sh,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("TH")) 
	                    	{ mettre_op(operations.op_th,4,parenthese);
	                    	i_val++;
	                    	}
	                    
	                    else if (str.equals("ARGSH")) 
	                    { mettre_op(operations.op_argsh,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("ARGCH")) 
	                    { mettre_op(operations.op_argch,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("ARGTH")) 
	                    { mettre_op(operations.op_argth,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("ABS")) 
	                    { mettre_op(operations.op_abs,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("FRAC"))
	                    { mettre_op(operations.op_frac,4,parenthese);
	                      i_val++;
	                    }
	                    else if (str.equals("INT")) 
	                    { mettre_op(operations.op_int,4,parenthese);
	                      i_val++;
	                    }
	                    else M_error=252;
	                    break;
	          default: M_error=251;           
	          }
	    }
	     
	    if ((M_error==0) && (mI_op+1!=i_val)) M_error=250;
	    N=mI_op;

	    // Détermination des priorités des opérateurs
	    // et tri du vecteur op
	     
	    for ( L=0 ; L<N ; L++ ) {
	          compt2=0;
	          do
	          {     compt=compt2;
	                while (mPriorite[compt]==9) compt++;
	                compt2=compt+1;
	                while (mPriorite[compt2]==9) compt2++;
	          }
	          while ( ( mPriorite[compt]<mPriorite[compt2] ) ||
	                 ( (mPriorite[compt]-(mPriorite[compt]/10)*10==4)&&
	                 (mPriorite[compt]==mPriorite[compt2]) ) );
	          mPriorite[compt]=9;
	          ordre1[L]=compt;
	          ordre2[L]=compt2;
	          operateurs[L]=mOp[compt];
	    };
	    //if ((Nx==0)&&(M_erreur==0))  M_Ans=de(0);
	     
	    // check syntax
	    // Vérifie que toutes les valeurs utilisées sont initialisées ou calculées
		compt=0;
		while ((compt<N)&&(M_error==0)) {
			int i=ordre2[compt];
			int p=ordre1[compt];
	        switch (operateurs[compt]) {
	        	case op_plus:
	        	case op_moins:
	        	case op_mul:
	        	case op_div:
	        	case op_xpy: if ((!IsInit[i]) || (!IsInit[p])) M_error=249; break;
	        	
	        	case op_sqr:
	        	case op_d_r:
	        	case op_fact: {	if (!IsInit[p]) M_error=248;
	        					IsInit[i] = true; } break;
	        					
	        	case rc:
	        	case op_sin:
	        	case op_cos:
	        	case op_tan:
	        	case op_ln:
	        	case op_log:
	        	case op_exp:
	        	case op_arcs:
	        	case op_ch: 
	        	case op_sh:	 
	        	case op_th:
	        	case op_abs:
	        	case op_frac:
	        	case op_int: if (!IsInit[i]) M_error=247; break;
	        		 
	        } // end switch
	        compt++;
	    } // end while

	    M_DefineError = M_error;
	} // end Define method
	
	
    //====================================================
    //
    //===================== Eval =========================
    //
    //====================================================
    	
	public double Eval(double x) {
		
		int compt=0;
		if (M_DefineError==0) M_error=0; else M_error=246;
		System.arraycopy( valeurs, 0, valr, 0, valeurs.length );	
		for (;compt<Nx ; compt++) valr[ x_places[compt] ] = x;
		
		compt=0;
		while ((compt<N)&&(M_error==0)) {
			
			int i=ordre2[compt];
	        int p=ordre1[compt];
	        switch (operateurs[compt]) {
	        	case op_plus :  valr[i]=valr[p]+valr[i]; break;
	        	case op_moins : valr[i]=valr[p]-valr[i]; break;
	        	case op_mul   : valr[i]=valr[p]*valr[i]; break;
	        	case op_div   : if (valr[i]!=0) valr[i]=valr[p]/valr[i];
	        	 				else M_error=1; //division par 0
	        	 				break;
	        	case rc       : if (valr[i]>=0) valr[i]=Math.sqrt(valr[i]);
                 				else M_error=2; //racine d"un nbr négatif 
                				break;
                				
	        	case op_sqr   : valr[i]=valr[p]*valr[p]; break;
	            case op_sin   : valr[i]=Math.sin(valr[i]); break;
	            case op_cos   : valr[i]=Math.cos(valr[i]); break;
	            case op_tan   : valr[i]=Math.tan(valr[i]); break;
	            case op_ln    : if (valr[i]>0) valr[i]=Math.log(valr[i]);
	                            else M_error=3; //logarithme d"un nombre <=0
	                            break;
	            case op_log   : if (valr[i]>0) valr[i]=Math.log10(valr[i]);
	                            else M_error=3;
	                            break;
	                            
	            case op_exp   : valr[i]=Math.exp(valr[i]); break;
	            case op_xpy   : if ( ((valr[p]<0)&&(valr[i]%1 !=0 )) ||
	                                 ((valr[p]==0)&&(valr[i]<=0)) )
	                               M_error=4;
	                            else valr[i]=Math.pow(valr[p],valr[i]);
	                            break;
	            case op_d_r   : valr[i]=Math.toRadians(valr[p]); break;
	            case op_arcs  : if ((valr[i]<=1)&&(valr[i]>=-1))
	                               valr[i]=Math.asin(valr[i]);
	                            else M_error=5;
	                            break;
	            case op_arcc  : if ((valr[i]<=1)&&(valr[i]>=-1))
	                               valr[i]=Math.acos(valr[i]);
	                             else M_error=6;
	                             break;
	            case op_arct  : valr[i]=Math.atan(valr[i]); break;
	            case op_ch    : valr[i]=Math.cosh(valr[i]); break;
	            case op_sh    : valr[i]=Math.sinh(valr[i]); break;
	            case op_th    : valr[i]=Math.tanh(valr[i]); break;
//	            case op_argsh : valr[i]=Math. argsh(valr[i]); break;
//	            case op_argch : if (valr[i]<1) M_erreur=7;
//	                               else valr[i]=argch(valr[i]);
//	                             break;
//	            case op_argth : if( fabs(valr[i])<1 ) valr[i]=argth(valr[i]);
//	                              else M_erreur=8;
//	                            break;
	            
	            case op_abs   : valr[i]=Math.abs(valr[i]); break;
	            case op_frac  : valr[i]=valr[i]%1; break;
	            case op_int   : valr[i]=Math.floor(valr[i]); break;
	            case op_fact  : if ((valr[p]>=0)&&(valr[p]%1==0)) 
	                               valr[i]=factorielle(valr[p]);
	                            else M_error=9;
	                            break;
//	            case op_ANP   : if ((frac(valeurs[i])==0)&&(frac(valeurs[p])==0)
//	                                &&(valeurs[i]>=0) && (valeurs[p]>=0)
//	                                &&(valeurs[p]>=valeurs[i]) )
//	                               valr[i]=anp( valr[p],valr[i] );
//	                            else M_erreur=10;
//	                            break;
//	            case op_CNP   : if ((frac(valeurs[i])==0)&&(frac(valeurs[p])==0)
//	                                &&(valeurs[i]>=0) && (valeurs[p]>=0)
//	                                &&(valeurs[p]>=valeurs[i]) )
//	                               valr[i]=cnp( valr[p],valr[i] );
//	                            else M_erreur=11;
//	                            break;
	                            
	        } // end switch
	        compt++;
		} // end while
		
		return(valr[N]);
	}
	

    //====================================================
    //
    //===================== Divers =========================
    //
    //====================================================
	
	public boolean FunctionOfX() { return (Nx>0); }
	
	private double factorielle(double n)
	{
	  int compt=1;
	  double fact=1.;
	  for ( ; compt<= n ; compt++) fact*=compt;
	  return(fact);
	};
	
	public double Aire( double a, double b, double epsilon )
	{
	   int i, n=1, m=0;
	   double h, Somme, Sold, Snew = 0.;
	   double S[] = new double[31];
	   
	       S[0]=(b-a)*( Eval(a) + Eval(b) )/2;
	       do
	       {
	             n=2*n;
	             h=(b-a)/n;
	             Somme = Eval(a+h);
	             i=3;
	             while (i<=n-1)
	             {
	               Somme += Eval(a+i*h);
	               i+=2;
	             }
	             Sold = S[0]/2 + h*Somme;
	             for (i=0; i<=m; i++ )
	             {
	               Snew = (Math.pow(4,i+1)*Sold-S[i])/(Math.pow(4,i+1)-1);
	               S[i]=Sold;
	               Sold=Snew;
	             };
	             m++;
	             S[m]=Snew;
	       }
	       while (( Math.abs(Snew-S[m-1])>=epsilon ) && ( m<30 ));
	       return(Snew);
	}

	static public void SetConstant(int i, double val) {
		if ((i>=0)&&(i<NConst)) constant[i]=val;
		
	}

	public String message_erreur() 
	{
	     switch (M_error) {
	     case   0 : return("");
	     case 255 : return("Syntax error");
	     case 254 : return("Syntax error : number format");
	     
	     
	     case 200 : return("Too complex expression");
	     case   1 : return("Division by 0");
	     case   2 : return("square root of negative number");
	     case   3 : return("Log of 0 or negative number");
	     case   4 : return("A^B with A<0 and B non int or A=0 and B<=0");
	     case   5 : return("ArcSin outside def.");
	     case   6 : return("ArcCos outside def.");
	     case   7 : return("ArgCh outside def.");
	     case   8 : return("ArgTh outside def.");
	     case   9 : return("Factorielle d'un nombre négatif ou non entier");
	     case  10 : return("(n A p) with incorrect n, p");		 
	     case  11 : return("(n C p) with incorrect n, p");		 
	     default  : return("");
	     } 
	}
	
} // end class MathFuncParser
