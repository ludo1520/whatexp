package fr.ludo1520.whatexp;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

//public class Settings extends PreferenceActivity {
public class Settings extends Activity {

	private CheckBox mCheckClearFunc;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_settings);
		
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);// if not the main activity, allows going back
		
		mCheckClearFunc = (CheckBox)findViewById(R.id.checkbx_clearFuncList);
		
		super.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_settings, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For

			ApplySettings();
			NavUtils.navigateUpFromSameTask(this);
			//Intent upIntent = NavUtils.getParentActivityIntent(this);
			//NavUtils.navigateUpTo(this, upIntent);

			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	
	@Override
	public void finish() {
		// quand le classique back button est pressé
		super.finish();
		ApplySettings();
		NavUtils.navigateUpFromSameTask(this);
	} 
	
	
	void ApplySettings() {
		
	    SharedPreferences settings = getSharedPreferences(WhatExp.PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
	    
	    if (mCheckClearFunc.isChecked()) {
	    	editor.putInt(WhatExp.N_FUNC_NAME,0);
	    	editor.commit();
	    }
		
	}
	
	
}
