package fr.ludo1520.whatexp;

/*
  Copyright 2014 Ludovic Gaudichet

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>
 */


// merci http://mickael-lt.developpez.com/tutoriels/android/personnaliser-listview/


import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;


public class functListFragment extends Fragment
	implements FuncAdapter.FuncAdapterListener {
	
	public static int NFuncMax = 200;
	funcListListener mCallBack; 
	ListView mListv;
	private ArrayList<String> mListFunc = new ArrayList<String>();
	private int mListColorDraw[] = new int[NFuncMax]; // array of color for drawing (0 = not selected)
	public int GetColorDraw(int i) { return mListColorDraw[i]; }
	
	public FuncAdapter mAdapter;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, 
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate( R.layout.fragment_funclist, container, false);
    }
    
    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
    	
    	mAdapter = new FuncAdapter(getActivity(), mListFunc);
    	mAdapter.SetListColorDraw(mListColorDraw);
    	mAdapter.addListener(this);
    	mListv = (ListView)getView().findViewById(R.id.ListFunction);
    	mListv.setAdapter(mAdapter);
    	
    	mListv.setMultiChoiceModeListener( new MultiChoiceModeListener() {

    		private int nselected = 0; 
    		
			@Override
			public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
				
				switch (item.getItemId()) {
				case R.id.item_delete: // in contextual_menu.xml
					RemoveSelection();
					mode.finish();
				}
				return false;
			}

			@Override
			public boolean onCreateActionMode(ActionMode mode, Menu menu) {
				MenuInflater inflater = getActivity().getMenuInflater();
				inflater.inflate(R.menu.contextual_menu, menu);
				return true;
			}

			@Override
			public void onDestroyActionMode(ActionMode mode) {
				nselected = 0;
				mAdapter.ClearSelection();
			}

			@Override
			public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
				return false;
			}

			@Override
			public void onItemCheckedStateChanged(ActionMode mode,
					int position, long id, boolean checked) {
				if (checked) ++nselected; else --nselected;
				mode.setTitle(nselected + " selected");
			}
    	});
    	
    	mListv.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
    	mListv.setOnItemLongClickListener(new OnItemLongClickListener() {
    	
    		@Override
    		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
    		int position, long arg3) {
    			mListv.setItemChecked(position,!mListv.isItemChecked(position));
    			return false;
    		}
    		});    	
    	
    	super.onActivityCreated(savedInstanceState);
    }

    public void AddToEnd(String str, int colorDraw) {
    	
    	int size = mListFunc.size();
    	if (size<NFuncMax) {
    		mListFunc.add(str);
    		mListColorDraw[size] = colorDraw;
    		if (mAdapter!=null) mAdapter.notifyDataSetChanged();
    	}
    }
    
    public void Add(String str, int colorDraw) {
    	int size = mListFunc.size();
    	if (size==NFuncMax) {
    		mListFunc.remove(NFuncMax-1);
    	}
    	int i = size-1;
    	while (i>0) {
    		mListColorDraw[i] = mListColorDraw[i-1];
    		--i;
    	}
    	mListColorDraw[0]=colorDraw;
    	mListFunc.add(0,str);
    	if (mAdapter!=null) mAdapter.notifyDataSetChanged();
    }
    
    public void RemoveSelection() {
    	
    	int size = mListFunc.size();
    	for (int i=size-1; i>=0;--i) {
    		if (mAdapter.mListSelected[i]) {
    			mListFunc.remove(i);
    			for (int j=i; j<size-1;++j) {
    				mListColorDraw[j]=mListColorDraw[j+1];
    			}			
    			--size;
    		}
    	}
    	if (mAdapter!=null) mAdapter.notifyDataSetChanged();
    }
    
    public int Nfunc() {return mListFunc.size();}
    
    public String GetFuncStr(int i) {
    	if ( (i>=0) && (i<mAdapter.getCount()) ) return (String)mAdapter.getItem(i);
    	else return null;
    }
    
    public String getSelectedExp() {
    	String str="";
    	int max = mListFunc.size();
    	for (int i=0; i<max;++i) {
    		if (mListColorDraw[i]!=0) str += (String)mAdapter.getItem(i)+'\n';
    	}
    	return str;
    }
    
    public boolean MustDraw(int i) {
    	if ( (i>=0) && (i<mListFunc.size()) ) return (mListColorDraw[i]!=0);
    	else return false;
    }
    
    public void Clear() {
    	
    	int i = mListFunc.size();
    	while (i>0) {
    		--i;
    		mListColorDraw[i] = 0;
    	}
    	mListFunc.clear();
    	mAdapter.notifyDataSetChanged();
    }
  
    /***************************************************
     * 
     * Container Activity must implement this interface
     * 
     ***************************************************/
    
    public interface funcListListener {
        public void onFuncSelected(String exp,int position);
    }
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (funcListListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


	/***************************************************
	 *  implements FuncAdapter.FuncAdapterListener
	 *  FuncAdapter calls these methods
	 ***************************************************/

	@Override
	public void onItemClick(String exp,int position) {
		mCallBack.onFuncSelected(exp, position);
	}

}

