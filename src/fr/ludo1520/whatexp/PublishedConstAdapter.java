package fr.ludo1520.whatexp;


/*
Copyright 2014 Ludovic Gaudichet

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>
*/


import java.util.ArrayList;

import android.content.Context;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PublishedConstAdapter extends BaseAdapter {
	
	private Listener mListener;
	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<String> mListName;
	private ArrayList<String> mListUnit;
	private ArrayList<String> mListValue;

	public PublishedConstAdapter(Context context) {

		mContext = context;
		mListName = new ArrayList<String>();
		mListUnit = new ArrayList<String>();
		mListValue = new ArrayList<String>();
		
		String publishedConst[]=context.getResources().getStringArray(R.array.publishedConst_array);
		mInflater = LayoutInflater.from(mContext);
		int i=0;
		while (i<publishedConst.length) {
			mListName.add(publishedConst[i++]);
			mListUnit.add(publishedConst[i++]);
			mListValue.add(publishedConst[i++]);
		}
	}
	
	
	@Override
	public int getCount() {
		return mListName.size();
	}

	@Override
	public Object getItem(int position) {
		return mListName.get(position);
	}
	
	public String getItemValue(int position) {
		return mListValue.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
//	@Override
//	public int getViewTypeCount() {
//	    return 2;
//	}
	
//	@Override
//	public int getItemViewType(int position) {
//		if (position==0) return 0;
//		else return 1;
//	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		RelativeLayout layoutItem;

		if (convertView == null) {
			layoutItem = (RelativeLayout) mInflater.inflate(R.layout.publishedconst_item,
						parent, false);
		} else {
		  	layoutItem = (RelativeLayout) convertView;
		}
				
		layoutItem.setTag(position);
		
		if (convertView == null)
			layoutItem.setOnClickListener( new OnClickListener() {
				@Override
				public void onClick(View v) {
					Integer position = (Integer)v.getTag();
					sendListener(position);		
					}
			});

		TextView whatTV = (TextView)layoutItem.findViewById(R.id.const_what);
		whatTV.setText(mListName.get(position));
		TextView valueTV = (TextView)layoutItem.findViewById(R.id.const_value);
		valueTV.setText(mListValue.get(position));
		TextView unitTV = (TextView)layoutItem.findViewById(R.id.const_unit);
		unitTV.setText(mListUnit.get(position));

		return layoutItem;
	}

	
	/**************************************************************
	 * 
	 * listener : define what an activity must implement in order to
	 * listen what is going on 
	 * 
	 **************************************************************/
	
	
	public interface Listener {
	    public void onPublishedConstClick(double val);
	}
	
	
	public void SetListener(Listener listener) {
		mListener = listener;
	}
	
	private void sendListener(int position) {
		String str = mListValue.get(position).replaceAll("\\s","");
		double val=Double.parseDouble(str);
		mListener.onPublishedConstClick(val);
	}

}
